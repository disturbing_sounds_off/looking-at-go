package main

import "fmt"

func main() {
	isValid("(){}[]")
}

func isValid(s string) bool {
	rez := false
	pars := map[string]int{"(": 0, "[": 0, "{": 0}
	for _, c := range s {
		key := string(c)
		if _, ok := pars[key]; ok {
			pars[key]++
			// } else if c == ')' {
			// 	pars[")"]--
			// }
		} else {
			pars[key]--
		}
		fmt.Printf("t1: %T\n", c)
	}
	fmt.Println(pars)
	return rez
}
