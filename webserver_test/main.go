package main

import (
	"fmt"
	"net/http"
)

func getHello(w http.ResponseWriter, r *http.Request) {
	fmt.Println("получен запрос на /hello")
	fmt.Fprintln(w, "Привет !")
}

func main() {
	fileServer := http.FileServer(http.Dir("./static"))
	http.Handle("/", fileServer)

	http.HandleFunc("/hello", getHello)

	http.HandleFunc("/show", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Hey Kolya !")
	})

	http.ListenAndServe(":3333", nil)
}

func test(sum int) (bool, x int, y int) {
	return
}
