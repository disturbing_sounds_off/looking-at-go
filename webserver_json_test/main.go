package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type Todo struct {
	name    string
	done    bool
	dueDate time.Time
}

func postRoot(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "test post \n")
}

func getRoot(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "test get \n")
}

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/", postRoot).Methods("POST")
	r.HandleFunc("/", getRoot).Methods("GET")

	srv := &http.Server{
		Addr:    ":3333",
		Handler: r,
	}
	srv.ListenAndServe()
}
