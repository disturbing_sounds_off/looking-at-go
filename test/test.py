
def isPalindrome(x: int) -> bool:
    if x < 0: 
        return False
    temp = x
    rev = 0
    while temp > 0:
        rev = rev * 10 + temp % 10
        temp = int (temp / 10)
        print(temp)

    return rev == x

print( isPalindrome(123) )
print( isPalindrome(121) )

