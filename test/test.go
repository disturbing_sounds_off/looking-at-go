package main

import (
	"fmt"
)

func main() {
    fmt.Println("Hello world")
    fmt.Println(isPalindrome(121))
}

func isPalindrome(x int) bool {

    if x < 0 {return false}

    temp := x
    rev := 0

    for temp > 0 {
        rev = rev * 10 + temp % 10
        temp /= 10
    }

    return rev == x

}
// func isPalindrome(x int) bool {
//     
// }
